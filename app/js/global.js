/* Author:http://www.rainatspace.com*/

function initializeScript(){
	//jQuery('#preloader').fadeOut(2000);
		jQuery("[icon-data]").each(function(){
		var getAttr =  jQuery(this).attr('icon-data');
		jQuery(this).addClass(getAttr).removeAttr('icon-data');
	});
}

/* =Document Ready Trigger
-------------------------------------------------------------- */
jQuery(document).ready(function(){
    initializeScript();

    
  	//FREEWALL
  	var wall = new freewall("#freewall");
	wall.reset({
		selector: '.brick',
		animate: true,
		cellW: 270,
		cellH: 'auto',
		gutterY: 20,
		onResize: function() {
			wall.fitWidth();
		}
	});
	wall.container.find('.brick img').load(function() {
		wall.fitWidth();
	});

    jQuery('#full-width-slider').royalSlider({
	   arrowsNav: true,
	    loop: false,
	    keyboardNavEnabled: true,
	    controlsInside: false,
	    imageScaleMode: 'fill',
	    arrowsNavAutoHide: false,
	    autoScaleSlider: true, 
	    autoScaleSliderWidth: 960,     
	    autoScaleSliderHeight: 350,
	    controlNavigation: 'bullets',
	    thumbsFitInViewport: false,
	    navigateByClick: true,
	    startSlideId: 0,
	    autoPlay: true,
	    transitionType:'move',
    	globalCaption: false,
	    deeplinking: {
	      enabled: true,
	      change: false
	    },
	    autoPlay: {
           enabled: true,
           //pauseOnHover: true,
           delay: 6000
        },
	    /* size of all images http://help.dimsemenov.com/kb/royalslider-jquery-plugin-faq/adding-width-and-height-properties-to-images */
	    imgWidth: 1100,
    	imgHeight: 380
  	});

  	jQuery('#slideBox').royalSlider({
	    autoHeight: true,
	    arrowsNav: true,
	    fadeinLoadedSlide: false,
	    arrowsNavAutoHide: false,
	    controlNavigationSpacing: 0,
	    controlNavigation: 'tabs',
	    imageScaleMode: 'none',
	    imageAlignCenter:false,
	    loop: true,
	    loopRewind: true,
	    numImagesToPreload: 6,
	    keyboardNavEnabled: true,
	    usePreloader: false,
	    autoPlay: {
           enabled: true,
           //pauseOnHover: true,
           delay: 4000
        },
	  });

  	//RESPONSIVE NAV
    jQuery(".open-btn").on("click", function() {
        jQuery("html").addClass("sideMenuOpened");
        //CLOSE ON CLICK OTHER AREA THAN NAV
        if (jQuery("html").hasClass("sideMenuOpened")) {
            jQuery("#main:not(.open-btn)").on("click", function() {
                jQuery("html").removeClass("sideMenuOpened");
            });
        }
    });
    jQuery(".close-btn").on("click", function() {
        jQuery("html").removeClass("sideMenuOpened");
    });


});
/* END ------------------------------------------------------- */